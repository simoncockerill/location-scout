import { html, LitElement } from '@polymer/lit-element';

export class LocationScout extends LitElement {
    constructor() {
        super();

        this.locationData = {};
        this.routeData = {};

        this._setupListeners();
        this.getDataFromLocation();
    }

    _setupListeners() {
        this.listeners = {
            click: (e) => {
                if (!e.defaultPrevented) {
                    e.preventDefault();
                }

                let el = e.path[0];
                let url = String(el.href);

                if (url == null | url == "undefined") return;

                if (!url.includes(this.basePath, 0)) {
                    if (this.allowExternalLinks) {
                        window.location = url;
                    } else {
                        window.open(url, '_blank');
                    }

                    return;
                }

                let urlData = this.populateLocationData(url);
                this.routeData = this.populateRouteData(urlData);

                window.history.pushState(null, '', '/' + urlData);
            },
            load: (e) => {
                this.basePath = location.protocol + '//' + location.host + '/';
                this.getDataFromLocation();
                let urlData = this.populateLocationData(window.location.href);
                this.routeData = this.populateRouteData(urlData);
                this.updateNotify();
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();

        window.addEventListener('load', this.listeners.load);
        document.addEventListener('click', this.listeners.click);
    }

    static get properties() {
        return {
            locationData: Object,
            routeData: Object,
            allowExternalLinks: Boolean
        };
    }

    _render() {
        return html ``;
    }

    _propertiesChanged(props, changed, oldProps) {
        if (changed && 'allowExternalLinks' in changed) {
            this.allowExternalLinks = changed.allowExternalLinks;
        } else {
            this.updateNotify();
        }

        super._propertiesChanged(props, changed, oldProps);
    }

    updateNotify() {
        let details = {
            locationData: this.locationData,
            routeData: this.routeData
        };
        this.dispatchEvent(
            new CustomEvent('location-updated', { bubbles: true, composed: true, detail: details })
        );
    }

    ready() {
        super.ready();
    }

    populateRouteData(path) {
        //split into LEVEL1/LEVEL2, {querystring: value}, fragmentData
        //example:
        //level1/level2/level3?query=1234&thing="blob"#bang
        if (path == null || path == "undefined") return;

        let routeData = {};

        //split by query
        let parts = path.split('?');
        let pathLevels = parts[0].replace(/\/$/, '');
        routeData.levels = pathLevels.split('/');

        if (parts.length > 1) {
            //split by #
            let dataParts = parts[1].split('#');

            //split by amp and equals
            routeData.query = this.convertQueryStringToObject(dataParts[0]);

            if (dataParts.length > 1) {
                routeData.fragment = dataParts[1];
            }
        }

        return routeData;
    }

    convertQueryStringToObject(query) {
        let decodedURI = decodeURI(query);
        let json = '{"' + decodedURI.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}';
        let queryObject = JSON.parse(json);
        return queryObject;
    }

    populateLocationData(url) {
        //get url without standard url parts that dont change eg. 'https://mydomain:80/'
        let path = url.replace(this.basePath, '');

        //split end of url into path '/blah/flap/' querystring '?this=that' and fragment '#bang'
        let urlParts = path.split('?');
        if (urlParts.length > 0) {
            this.locationData.path = urlParts[0];
            if (urlParts.length > 1) {
                let urlSearch = urlParts[1].split('#');
                this.locationData.queryString = urlSearch[0];
                if (urlSearch.length > 1) {
                    this.locationData.fragment = urlSearch[1];
                } else {
                    this.locationData.fragment = '';
                }
            } else {
                this.locationData.queryString = '';
                this.locationData.fragment = '';
            }
        }

        return path;
    }

    getDataFromLocation() {
        let loc = window.location;
        this.protocol = loc.protocol;
        this.host = loc.host;
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener('load', this.listeners.load);
        document.removeEventListener('click', this.listeners.click);
    }
}

customElements.define('location-scout', LocationScout);